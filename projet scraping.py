import urllib.request
from bs4 import BeautifulSoup
import csv

site = 'https://www.lefigaro.fr/'

request = urllib.request.Request(site)

scraper = urllib.request.urlopen(request)

parse = BeautifulSoup(scraper, 'html.parser')

texte1 = parse.find_all('h3', attrs={'class': 'css-5pe77f'})
texte2 = parse.find_all('p', attrs={'class': 'css-hjukut'})

with open('projscrap.csv', 'a') as csv_file:
  writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
  writer.writerow(['Titre','Auteur'])
  for col1,col2 in zip(texte1, texte2):
    writer.writerow([col1.get_text().strip(), col2.get_text().strip()])